package uac;

public class UniversidadPagos {

    private datos inicio;
    private datos fin;
    
    public UniversidadPagos(){
        inicio=fin=null;
    }
    
    public boolean estaVacia(){
        return inicio == null;
    }
    
    
        public void agregarInicio(int elemento){
            if (!estaVacia()){
                inicio= new datos(elemento, inicio, null);
                inicio.siguiente.anterior = inicio;
            }else{
                inicio=fin=new datos(elemento);
            }
        }
    
        public void agregarFinal(int elemento){
            if (!estaVacia()){
                fin= new datos(elemento, null, fin);
                fin.anterior.siguiente = fin;
            }else{
                inicio=fin=new datos(elemento);
            }
        }


        public void ListaInicioaFin(){
            if (!estaVacia()){
                String conector = "<=>";
                String datos = "<-";
                datos auxiliar = inicio;
                        while(auxiliar !=null){
                           datos = datos +"["+auxiliar.dato+"] "+conector;
                           auxiliar = auxiliar.siguiente;
                        }
                datos += "->"; 
                System.out.println(datos);
            }
        }

        public void ListaFinaInicio(){
            if (!estaVacia()){
                String conector= "<=>";
                String datos = " ";
                datos auxiliar = fin;
                    while(auxiliar !=null){
                       datos = datos +"["+auxiliar.dato+"]"+conector;
                       auxiliar = auxiliar.anterior;
                    }
                System.out.println(datos);
            }
        } 

        public int borrarInicio(){
            int elemento = inicio.dato;
            if (inicio == fin){
                inicio=fin=null;
            }else{
                inicio = inicio.siguiente;
                inicio.anterior= null;
            }
            return elemento;
        }

        public int borrarFinal(){
            int elemento = fin.dato;
            if (inicio == fin){
                inicio=fin=null;
            }else{
                fin = fin.anterior;
                fin.siguiente= null;
            }
            return elemento;
        }
                //metodo para eliminar un nodo especifico
                public void EliminarNodoEspecifico(int elemento){
            if(!estaVacia()){
                if(inicio==fin && elemento==inicio.dato){
                    inicio=fin=null;
                }else if(elemento==inicio.dato){
                    inicio=inicio.siguiente;
                }else{
                    datos anterior,temporal;
                    anterior=inicio;
                    temporal=inicio.siguiente;
                    while(temporal!=null && temporal.dato!=elemento){
                    anterior=anterior.siguiente;
                    temporal=temporal.siguiente;
                }
                    if(temporal!=null){
                       anterior.siguiente=temporal.siguiente;
                       if(temporal==fin){
                           fin=anterior;
                    }
                }
            }
        }
    }

    
    public static void main(String[] args) {
        long inicio = System.nanoTime();

                UniversidadPagos Lista = new UniversidadPagos();
                fila(Lista);
                Lista.ListaFinaInicio();
                Lista.borrarInicio();
                Lista.ListaFinaInicio();
                /*necesita el nodo donde esta el inquieto
                Lista.EliminarNodoEspecifico("aqui");*/
                System.out.println("El ultimo alumno  de la fila se fue ");
                    long fin = System.nanoTime();
                    double ta =(double)(fin-inicio)*30000.0e-9;
                    System.out.println("El tiempo que se tardo en atender toda la fila fueron:"+ ta+ " segundos");
    
}

        public static void fila(UniversidadPagos NodoDoble){
            int na=alumnos.gna(10, 50);
                System.out.println("En la fila hay un total de "+na+" alumnos");

                tiempo p;
                int segundos;
                for (int i = 10; i < na; i++) {
                    
                    segundos=alumnos.gna(10, 15);
                    p = new tiempo(segundos);
                    System.out.println("Se ha integrado un nuevo alumno a la fila en:"+p.getsegundos()+" segundos");
                    NodoDoble.agregarInicio(i);
                }
                //intranquilo
                for (int in = na-2; in < na; in++) {
                    
                    segundos=alumnos.gna(10, 15);
                    p = new tiempo(segundos);
                    System.out.println("Se ha integrado un nuevo alumno intranquilo a la fila en:"+p.getsegundos()+" segundos");
                    NodoDoble.agregarInicio(in);
                }
            }
}
